## Python Snippets

The example programs in this repository are intended to help engineers master their every day work more efficiently and to ease the transition from other scripting languages such as MATLAB. The code examples are grouped into the following categories:

 - [excel](https://bitbucket.org/modelon/pythonsnippets/raw/master/excel) - read and write excel files
 - [mat](https://bitbucket.org/modelon/pythonsnippets/raw/master/mat) - read and write MAT files
 - [numpy](https://bitbucket.org/modelon/pythonsnippets/raw/master/numpy) - numpy snippets
 - [misc](https://bitbucket.org/modelon/pythonsnippets/raw/master/misc) - iteration, functions, if and for loops
 - [apps](https://bitbucket.org/modelon/pythonsnippets/raw/master/apps) - sample applications

Before you start to dig into the example programs we'd like to point you to some great resources on the web to help you getting started with Python.

 - [NumPy for MATLAB users](http://wiki.scipy.org/NumPy_for_Matlab_Users) - a good starting point for MATLAB users including a table with [MATLAB-NumPy equivalents](http://wiki.scipy.org/NumPy_for_Matlab_Users#head-5a9301ba4c6f5a12d5eb06e478b9fb8bbdc25084)
 - [NumPy Tutorial](http://wiki.scipy.org/Tentative_NumPy_Tutorial) - a more in-depth introduction to NumPy
 - [matplotlib gallery](http://matplotlib.org/1.3.1/gallery.html) - hundreds of plot examples

Most of the examples require additional libraries. The easiest way to get all the libraries you need is to install [Anaconda](https://store.continuum.io/cshop/anaconda/), a Python distribution for Windows, Mac and Linux. Although most Python distributions come with a basic editor we recommend to use [PyCharm](https://www.jetbrains.com/pycharm/) as a development environment.

If you think a link is missing or you would like to share a snippet please send us a [pull request](https://confluence.atlassian.com/display/BITBUCKET/Work+with+pull+requests)!

All code in this repository is provided by Modelon as free software under the BSD license so you can use it in your projects.
