'''
Dump the contents of a MAT file to the command line
'''

import scipy.io
import os
import numpy
import sys

def dump_mat(filename):    
    
    # print the file name
    print os.path.split(filename)[1]
    
    # load the MAT file
    mat = scipy.io.loadmat(filename)
    
    for key, value in mat.items():
        print
        if isinstance(value, numpy.ndarray):
            print key + ' '  + str(value.dtype) + ' ' + str(value.shape)
            print str(value)
        else:
            print key + ': ' + str(value)
    
if __name__ == '__main__':
    
    # skip the first argument
    sys.argv.pop(0)
    
    # iterate over the rest
    for arg in sys.argv:
        dump_mat(arg)
