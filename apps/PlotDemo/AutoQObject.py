'''
A factory for dynamic QObjects with Properties
'''

from PySide import QtCore

def AutoQObject(*class_def, **kwargs):
    
    class Object(QtCore.QObject):
        
        def __init__(self, **kwargs):
            QtCore.QObject.__init__(self)
            for key, val in class_def:
                self.__dict__['_'+key] = kwargs.get(key, type(val))

        def __repr__(self):
            values = ('%s=%r' % (key, self.__dict__['_'+key]) \
                    for key, value in class_def)
            return '<%s (%s)>' % (kwargs.get('name', 'QObject'), ', '.join(values))
        
        for key, value in class_def:
            nfy = locals()['_nfy_'+key] = QtCore.Signal(value)

            def _get(key):
                def f(self):
                    #print 'called: _get_%s' % key
                    return self.__dict__['_'+key]
                return f

            def _set(key):
                def f(self, value):
                    #print 'called: _set_%s(%s)' % (key, value)
                    self.__dict__['_'+key] = value
                    self.__dict__['_nfy_'+key].emit(value)
                return f

            set = locals()['_set_'+key] = _set(key)
            get = locals()['_get_'+key] = _get(key)

            locals()[key] = QtCore.Property(value, get, set, notify=nfy)

    return Object

# Car = AutoQObject(
#     ('model', str),
#     ('brand', str),
#     ('year', int),
#     ('inStock', bool),
#     name='Car'
# )
# 
# def printString(s):
#     print s
# 
# c = Car(model='Fiesta', brand='Ford', year=1337)
# 
# c._nfy_model.connect(printString)
# 
# c._set_model('Focus')
# 
# pass
