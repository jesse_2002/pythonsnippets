## Build the standalone app

> python build.py PlotDemo.spec


## I18N

The translation files in /i18n are automatically generated from the project file by running

> pyside-lupdate.exe -noobsolete PlotDemo.pro

lupdate creates / updates every file specified in TRANSLATIONS with the strings found in the
source code and forms. `-noobsolete` revmoves the translations that are not used anymore.

After adding the translations with linguist.exe run `File` > `Release` to generate the 
*.qm files. To change the language set the environment variable `LANG` in autoexec.bat or run

> set LANG=fr